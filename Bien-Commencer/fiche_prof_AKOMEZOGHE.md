Thématique:Création d'une page web personnelle
Notions liées: HTML, CSS, balises HTML pour organiser le contenu de la page et ajouter du contenu textuel et multimédia à la page, propriété CSS pour styliser les éléments HTML
Résumé de l'activité: C'est une activité branché, les élèves devront réaliser une page web simple dont ils auront le modèle. Ils utiliseront un éditeur de texte et afficheront ensuite leur page sur un navigateur pour vérifier que leur réalisation correspond au modèle.
Objectif de l'activité: Cette activité permet aux élèves de développer leurs compétences en programmation web tout en encourageant la créativité et la collaboration. Elle leur offre également une opportunité de réfléchir à leur apprentissage et de partager leurs réalisations.
Auteur: Gwladys Akomezoghe
Durée de l'activité: 2H
forme de participation: individuelle ou en binôme
Matériel nécessaire: un ordinateur, un éditeur de texte, un navigateur
fiche élève à cette adresse: https://gitlab.com/gwladys2/mooc-2-ressources/-/blob/main/Bien-Commencer/fiche_eleves.pdf